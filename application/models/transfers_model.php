<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'models/abstractdbtable_model.php'; // Потому что только так цеплять абстрактные классы

class Transfers_Model extends AbstractDBTable_Model
{
    /**
     * Имя таблицы где данные
     * @var string
     */
    protected $table = 'transfers';
    /**
     * Процент в процента коммиссии за транзакцию
     * @var float
     */
    private $commission_procent = 0.99;
    /**
     * Куда отправлять коммисию , системый акк - номер счета
     * @var int
     */
    private $system_serial = 0;

    /**
     * Консипрутор класса
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Получить трансферы , исходящие и входящие по номеру счета
     * @param $serial - номер счета
     * @return mixed массив транзакциями
     */
    public function getTransfers($serial)
    {
        $this->db->select(
            array($this->getTable() . '.id',
                $this->getTable() . '.from_serial',
                $this->getTable() . '.to_serial',
                $this->getTable() . '.sum',
                $this->getTable() . '.datetime',
                $this->getTable() . '.from_new_balance',
                $this->getTable() . '.to_new_balance',
                'a1.client as a1client',
                'a2.client as a2client')
            , FALSE
        );
        $this->db->where('from_serial = ', $serial);
        $this->db->or_where('to_serial = ', $serial);
        $this->db->order_by('datetime', 'abs');
        $this->db->join($this->accounts_model->getTable() . ' as a1', $this->getTable() . '.from_serial =a1.serial', 'left');
        $this->db->join($this->accounts_model->getTable() . ' as a2', $this->getTable() . '.to_serial =a2.serial', 'left');
        return $this->db->get($this->table)->result_array();
    }

    /**
     * Переводим деньги в количестве sum с $from на $to
     * @param $from_serial номер счета отправильтеля
     * @param $to_serial номер счета получателя
     * @param $sum сумма
     */
    private function transfer($from_serial, $to_serial, $sum)
    {
        try {
            $from_new_balance = $this->accounts_model->subToBalanceBySerial($from_serial, $sum);
            $to_new_balance = $this->accounts_model->addToBalanceBySerial($to_serial, $sum);
        } catch (ErrorException $e) {
            throw new ErrorException($e->getMessage());
        }
        $this->db->insert($this->table, array(
            'from_serial' => $from_serial,
            'to_serial' => $to_serial,
            'sum' => $sum,
            'datetime' => date('Y-m-d H:i:s'),
            'from_new_balance' => $from_new_balance,
            'to_new_balance' => $to_new_balance
        ));
    }

    /**
     * Перевод денег но с коммиссией, в случае проблем транзакциия откатывается
     * @param $from_serial  номер счета отправильтеля
     * @param $to_serial омер счета получателя
     * @param $sum сумма
     * @return bool резулатат, до false не должно доходить ибо ексепшен должны отрабатывать.
     * @throws ErrorException выбрасываем екшепшен если что-то пошло не так
     */
    public function transferWithCommission($from_serial, $to_serial, $sum)
    {
        // Например если перевести 10 у.е то коммисия системы получается 0,099 но база настолько умная что округляет по правилам до 0.1
        $commission_money = ($sum / 100) * $this->commission_procent;
        $all_money = $sum + $commission_money;
        $cur_from_balance = $this->accounts_model->getBalanceBySerial($from_serial);
        if (($cur_from_balance - $all_money) < 0) {
            // А после вычета суммы перевода и коммисси меньше нуля получается
            throw new ErrorException('Мало денег на балансе у отправителя');
            return false;
        }
        $this->db->trans_start();
        try {
            $this->transfer($from_serial, $to_serial, $sum);
            $this->transfer($from_serial, $this->system_serial, $commission_money);
        } catch (ErrorException $e) {
            // Откатываем нафиг если что-то пошло не так
            $this->db->trans_rollback();
        }
        $this->db->trans_complete();
        return true;
    }
}