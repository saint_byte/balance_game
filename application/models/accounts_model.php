<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
require_once APPPATH . 'models/abstractdbtable_model.php';

class Accounts_Model extends AbstractDBTable_Model
{
    /**
     * ��� ������� � ������� ��������
     * @var string
     */
    protected $table = 'accounts';

    /**
     * ����������� ������
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * ������� �������
     * @param $client - ������������ �������
     * @param $serial - ����� �����
     * @param $balance - ��������� ������
     * @return bool TRUE ���� ��� ������ ������( � �� ������� � ���� �� �����)
     */
    public function createAccount($client, $serial, $balance)
    {
        $filter_data = array(
            'client' => $client,
            'serial' => $serial,
            'balance' => $balance
        );
        $this->db->insert($this->table, $filter_data);
        return true;
    }

    /**
     *  �������� ��� ������ � ������� �� ������ �����
     * @param $serial
     * @return ������ � ������� � �������
     */
    public function getBySerial($serial)
    {
        $qh = $this->db->where('serial', $serial)->get($this->table);
        if ($qh->num_rows == 0) return false;
        $res = $qh->result_array();
        return $res[0];
    }

    /**
     * ������������� ����� ������
     * @param $serial - ����� �����
     * @param $balance - ����� ������
     */
    private function setBalanceBySerial($serial, $balance)
    {
        $this->db->where('serial', $serial)->update($this->table, array('balance' => $balance));
    }

    /**
     * �������� ������ �� ������ �����
     * @param $serial ����� �����
     * @return mixed ������� ������
     */
    public function getBalanceBySerial($serial)
    {
        $account_data = $this->getBySerial($serial);
        return $account_data['balance'];
    }

    /**
     * �������� ������ �� ������
     * @param $serial - ����� �����
     * @param $sum ����� ������� ��������
     * @return mixed ����� ������
     */
    public function addToBalanceBySerial($serial, $sum)
    {
        return $this->calcBalanceBySerial($serial, $sum, '+');
    }

    /**
     * ������ ������ � �������
     * @param $serial - ����� �����
     * @param $sum - ����� ������� �������
     * @return mixed - ����� ������
     */
    public function subToBalanceBySerial($serial, $sum)
    {
        return $this->calcBalanceBySerial($serial, $sum, '-');
    }

    /**
     * ������ � ��������
     * @param $serial - ����� �����
     * @param $sum - �����
     * @param $action - ��� ������� , - ������� �����, + ��������
     * @return mixed ����� ������
     */
    private function calcBalanceBySerial($serial, $sum, $action)
    {
        $account_balance = $this->getBalanceBySerial($serial);
        if ($action == '-') {
            $new_balance = $account_balance - $sum;
        } else {
            $new_balance = $account_balance + $sum;
        }
        $this->setBalanceBySerial($serial, $new_balance);
        return $new_balance;
    }

} 