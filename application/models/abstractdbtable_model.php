<?php

/**
 * Class AbstractDBTable_Model
 * Класс от которого в дальнешйшем будут порождаться другие классы , по сути абстрактный ,
 * но методов которые надо реализовывать в каждом классе по своему я пока не придумал ,
 * но надо будет - если например придется прятать системный акк
 */
class AbstractDBTable_Model extends CI_Model
{

    /**
     * Имя таблицы с которой работает класс
     * @var string
     */
    protected $table;

    /**
     * Конструктор класса , чтоб он был нормальной моделью , вызывает конструктр предка
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Получить имя таблицы
     * @return mixed
     */
    public function  getTable()
    {
        return $this->table;
    }

    /**
     * Получить все данные из таблицы
     * @return mixed
     */
    public function get_all()
    {
        $q = $this->db->get($this->table);
        return $q->result_array();
    }

    /**
     * Получить количество записей в таблице
     * @return mixed
     */
    public function recordСount()
    {
        return $this->db->count_all($this->table);
    }

    /**
     * Получить данные в заданном диапозоне
     * @param $start с какой записи начать
     * @param $limit сколько записей получить
     * @return bool - если все прошло норлмально получаем массив , если нет то FALSE
     */
    public function fetchWithLimit($start, $limit)
    {
        $this->db->limit($limit, $start);
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) return $query->result_array();
        return false;
    }


} 