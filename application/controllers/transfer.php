<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Transfer
 *  Контроллер который занимается транзакциями
 */
class Transfer extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Акшен Создания перевода, одна из тех фунцкий которые даже падать должны с откатываем
     * Показывает форму , выполняет простые проверки , отображает ошибку если что-то пошло не так
     */
    public function create()
    {
        $title = 'Форма перевода';
        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');

        $accs_arr = $this->accounts_model->get_all();
        $this->form_validation->set_rules('from', 'Счет, откуда перевести', 'integer|required|callback_src_dest_check');
        $this->form_validation->set_rules('to', ' Счет, куда перевести', 'required|integer');
        $this->form_validation->set_rules('sum', 'Сумма', 'required|numeric');
        $this->form_validation->set_error_delimiters('<div class="error"><div>', '</div></div>');
        $this->load->view('header', array('title' => $title));
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('transfer/create', array('accounts' => $accs_arr, 'title' => $title));
        } else {
            // единственное место где надо чтобы все было четко  =)
            $error = false;
            $err_msg = '';
            try {
                $this->transfers_model->transferWithCommission($_POST['from'], $_POST['to'], $_POST['sum']);
            } catch (ErrorException $e) {
                $error = true;
                $err_msg = $e->getMessage();
            }
            $this->load->view('transfer/created', array('title' => $title, 'error' => $error, 'error_message' => $err_msg));
        }
        $this->load->view('footer');
    }

    /**
     * валидатор форму , проверяет что отправитель не равен получателю
     * @param $str - Значение которые валидируем (в данном случае это from)
     * @return bool - результат
     */
    public function src_dest_check($str)
    {
        if ($str == $_POST['to']) {
            $this->form_validation->set_message('src_dest_check', 'нельзя самому себе пересылать деньги');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}