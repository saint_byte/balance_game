<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Class Accounts
 * Класс контроллера для отображения списка счетов, создания счетов , детализации по счету
 */
class Accounts extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *  Акшен который занимается создание нового клиента(счета ему) , проверяет и показывает форму для создания клиента
     */
    public function create()
    {
        $data['title'] = 'Форма создания счета';
        $this->load->helper(array('form', 'url'));
        $this->load->view('header', $data);
        $this->load->library('form_validation');

        $this->form_validation->set_rules('client', 'Имя клиента', 'trim|required|min_length[3]|max_length[128]');
        $this->form_validation->set_rules('serial', 'Номер', 'required|integer|is_unique[accounts.serial]');
        $this->form_validation->set_rules('balance', 'Баланс', 'required|numeric');
        $this->form_validation->set_error_delimiters('<div class="error"><div>', '</div></div>');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('accounts/create', $data);
        } else {
            $this->accounts_model->createAccount($_POST['client'], $_POST['serial'], $_POST['balance']);
            $this->load->view('accounts/created', $data);
        }
        $this->load->view('footer');
    }

    /**
     * Акшен показывает список клиентов , если клиентов много врубается паггинация
     */
    public function list_accounts()
    {
        $data['title'] = 'Страница со клиентами';
        $this->load->helper('url');
        $config = array();
        $config["base_url"] = base_url() . "accounts?";
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'begin';
        $config["total_rows"] = $this->accounts_model->recordСount();
        $config["per_page"] = 20;
        $this->pagination->initialize($config);
        $begin = intval(@$_GET['begin']); // В деве вылазитт нотис
        $data["results"] = $this->accounts_model->fetchWithLimit($begin, $config["per_page"]);
        $data["links"] = $this->pagination->create_links();
        $this->load->view('header', $data);
        $this->load->view('accounts/list_accounts', $data);
        $this->load->view('footer');
    }

    /**
     * Показывает транзакции на счете клиенте
     * @param $serial - номер счета клиента
     */
    public function list_transfers($serial)
    {
        $accs_data = $this->accounts_model->getBySerial($serial);
        $title = 'Страница со клиентами / ' . $accs_data['client'];
        $data = array();
        $data['results'] = $this->transfers_model->getTransfers($serial);
        $data['serial'] = $serial;
        $data['title'] = $title;
        $this->load->view('header', array('title' => $title));
        $this->load->view('accounts/list_transfers', $data);
        $this->load->view('footer');
    }
}