<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" media="all" href="/static/css/style.css"/>
    <title><?php if (isset($title)) {
            echo $title;
        } else {
            'Система переводов';
        } ?></title>
</head>
<body>
<div class="conteiner">
    <div class="title"><span><?php if (isset($title)) {
                echo $title;
            } else {
                'Система переводов';
            } ?></span></div>
    <div class="menu">
        <ul>
            <li><a href="/createAccount/">Форма создания счета</a></li>
            <li><a href="/transfer/">Форма перевода</a></li>
            <li><a href="/accounts/">Страница со клиентами</a></li>
        </ul>
    </div>
    <div class="clearfix"></div>