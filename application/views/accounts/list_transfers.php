<a href="/accounts/">&lt; Назад</a><br />
<?php if (count($results) > 0) { ?>
<table class="listtable" >
    <thead>
    <tr>
        <th>дата/время операции</th>
        <th>счет отправителя или получателя</th>
        <th>приход/расход</th>
        <th>сумма</th>
        <th>остаток</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($results as $trn) {  ?>
        <tr>

            <td><?php echo date('d.m.Y H:i:s',strtotime($trn['datetime'])); ?></td>
            <td>
                <?php if ($trn['from_serial'] == $serial) { ?>
                    Получатель:<?php echo $trn['a2client']; ?>
                <?php } else { ?>
                    Отправитель:<?php echo $trn['a1client']; ?>
                <?php } ?>

            </td>
            <td>
                <?php if ($trn['from_serial'] == $serial) { ?>
                    расход
                <?php } else { ?>
                    приход
                <?php } ?>
            </td>
            <td>
                <?php echo $trn['sum']; ?>
            </td>
            <td>
                <?php if ($trn['from_serial'] == $serial) { ?>
                    <?php echo $trn['from_new_balance']; ?>
                <?php } else { ?>
                    <?php echo $trn['to_new_balance']; ?>
                <?php } ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<?php } else { ?>
    <div class="message">
        Пока нет переводов. <a href="/transfer/">Перевести</a>
    </div>
<?php } ?>