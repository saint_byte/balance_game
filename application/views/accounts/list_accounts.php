<?php if (count($results) > 0) { ?>
<table class="listtable" cellspaning="0" cellpadding="0" >
    <thead>
    <tr>
        <th>Имя клиента</th>
        <th>Номер</th>
        <th>Баланс</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($results as $item) { ?>
        <tr>
            <td>
                <a href="/accounts/<?php echo $item['serial']; ?>"><?php echo $item['client']; ?></a>
            </td>
            <td>
                <?php echo $item['serial']; ?>
            </td>
            <td>
                <?php echo $item['balance']; ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<p><?php echo $links; ?></p>
<?php } else { ?>
    <div class="message">
    Пока нет не одного клиента. <a href="/createAccount/">Создать первого</a>
    </div>
<?php } ?>