<?php if ($error) { ?>
    <div class="error">
        <div>Перевод не прошел: <?php echo $error_message; ?> <a href="/transfer/">Вернуться</a></div>
    </div>
<?php } else { ?>
<div class="message">
    Перевод прошел.
    <a href="/transfer/">Перевести ещё</a>
</div>
<?php } ?>