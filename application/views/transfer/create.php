<?php echo validation_errors(); ?>
<div>
    <form method="post">
        <div>
            <div>Счет, откуда перевести:</div>
            <div><select name="from">
                    <?php foreach ($accounts as $a) { ?>
                        <option
                            value="<?php echo $a['serial'] ?>" <?php echo set_select('from', $a['serial']); ?> ><?php echo $a['client'] ?></option>
                    <?php } ?>
                </select></div>
        </div>
        <div>
            <div>Счет, куда перевести:</div>
            <div>
                <select name="to">
                    <?php foreach ($accounts as $a) { ?>
                        <option
                            value="<?php echo $a['serial'] ?>" <?php echo set_select('to', $a['serial']); ?> ><?php echo $a['client'] ?></option>
                    <?php } ?>
                </select></div>
        </div>
        <div>
            <div>Сумма:</div>
            <div><input type="text" name="sum" value="<?php echo set_value('sum'); ?>"/></div>
        </div>
        <div>
            <div><input type="submit" value="Перевести" name="transfer"/></div>
        </div>
    </form>
</div>